﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using BizObjects._2c2p;

namespace WebApi._2c2p.Common
{
    public class Helper
    {
        public static HttpResponseMessage WarningResponse(HttpRequestMessage Request, object results)
        {
            WarningResponse resp = new WarningResponse();
            resp.status = "WARN";
            resp.results = results;
            return Request.CreateResponse((HttpStatusCode)422, resp);
        }

        public static HttpResponseMessage OKResponse(HttpRequestMessage Request, object results)
        {
            OKResponse resp = new OKResponse();
            resp.status = "OK";
            resp.results = results;
            return Request.CreateResponse((HttpStatusCode)200, resp);
        }

        public static HttpResponseMessage CustomErrorResponse(HttpRequestMessage Request, string errorCode, string errorMsg, Dictionary<String, object> errorDetails)
        {
            ErrorResponse errResults = new ErrorResponse();
            errResults.status = "ERR";
            errResults.errorCode = errorCode;
            errResults.errorMessage = errorMsg;
            errResults.errorDetails = errorDetails;

            return Request.CreateResponse((HttpStatusCode)200, errResults);
        }

    }

    public enum ErrorCode
    {
        Bad_Request = 400,
        Method_Not_Allowed = 405,
        Internal_Server_Error = 500,
        Service_Unavailable = 503,
    }
}