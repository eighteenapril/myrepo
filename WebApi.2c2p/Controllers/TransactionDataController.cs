﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BizObjects._2c2p;
using BizLogic._2c2p;
using WebApi._2c2p.Common;
namespace WebApi._2c2p.Controllers
{
    public class TransactionDataController : ApiController
    {
        /*[HttpGet]
        public IEnumerable<Response_TransactionData> GetAllProducts()
        {
            return TransactionDataBL.TransactionDataList();
        }
        */

        [HttpGet]
        public HttpResponseMessage GetAllTransactionData()
        {
            return Helper.OKResponse(Request, TransactionDataBL.TransactionDataList());
        }

        /*
        [HttpGet]
        public HttpResponseMessage GetTransactionDataByCurrency(string CurrencyCode)
        {
            return TransactionData.TransactionDataByCurrency(Request,CurrencyCode);
        }
        [HttpGet]
        public HttpResponseMessage GetTransactionDataByDateRange(string startDate, string endDate)
        {
            return TransactionData.TransactionDataByDateRange(Request, startDate,endDate);
        }
        [HttpGet]
        public HttpResponseMessage GetTransactionDataByStatus(string status)
        {
            return TransactionData.TransactionDataByStatus(Request, status);
        }
        */
    }
}
