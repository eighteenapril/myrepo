﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BizObjects._2c2p;
using DataAccess._2c2p;
using Dapper;

namespace BizLogic._2c2p
{
    public class TransactionDataBL
    {
        public static IEnumerable<Response_TransactionData> TransactionDataList()
        {
            return DapperCRUD.ReturnList<Response_TransactionData>("TransactionDataList", null);
        }
        public static IEnumerable<Response_TransactionData> TransactionDataByCurrency()
        {
            return DapperCRUD.ReturnList<Response_TransactionData>("TransactionDataList", null);
        }
        public static IEnumerable<Response_TransactionData> TransactionDataByDateRange()
        {
            return DapperCRUD.ReturnList<Response_TransactionData>("TransactionDataList", null);
        }
        public static IEnumerable<Response_TransactionData> TransactionDataByStatus()
        {
            return DapperCRUD.ReturnList<Response_TransactionData>("TransactionDataList", null);
        }

        //public static IEnumerable<TransactionData> TransactionDataByCurrency(string CurrencyCode)
        //{
        //    DynamicParameters param = new DynamicParameters();
        //    param.Add("@@CurrencyCode", CurrencyCode);
        //    return DapperCRUD.ReturnList<TransactionData>("TransactionDataByCurrency", param);
        //}
        //public static void (Employee Emp)
        //{
        //    DynamicParameters param = new DynamicParameters();
        //    param.Add("@EmpId", Emp.EmpId);
        //    param.Add("@EmpName", Emp.EmpName);
        //    param.Add("@@EmpAge", Emp.EmpAge);
        //    param.Add("@EmpSalary", Emp.EmpSalary);
        //    DataAccess.DapperCRUD.ExecuteWithoutReturn("AddNewEmployee", param);
        //}
    }
}
