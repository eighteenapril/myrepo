﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BizObjects._2c2p;
using System.IO;
using System.Xml;
using Microsoft.VisualBasic.FileIO;
using DataAccess._2c2p;
using Dapper;
namespace BizLogic._2c2p
{
    public class DataFileBL
    {
        public static string ProcessFile(string filePath, string extension)
        {
            try
            {
                string returnMsg = null;
                if (!string.IsNullOrEmpty(filePath))
                {
                    if (extension.ToLower().Equals(@".csv"))
                    {
                        returnMsg = CSVFileReader(filePath);                        
                    }
                    else
                    {
                        returnMsg = XMLFileReader(filePath);                         
                    }
                    return "File Uploaded Successfully!!";
                }
                return "File Uploaded Successfully!!";
            }
            catch (Exception ex)
            {
                return string.Format("Failed to upload file! {0}", ex.Message);
            }            
        }
        private static string CSVFileReader(string filePath)
        {
            try
            {
                List<TransactionData> Data = new List<TransactionData>();
                using (TextFieldParser parser = new TextFieldParser(filePath))
                {                    
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {                       
                        TransactionData TranData = new TransactionData();
                        string[] fields = parser.ReadFields();
                        TranData.TransactionId = fields[0] == null ? "" : fields[0];
                        TranData.TransactionDate = fields[3] == null ? Convert.ToDateTime(null) : Convert.ToDateTime(fields[3]);

                        if (fields[4] != null)
                        {
                            if (fields[4].ToLower().Equals("approved")) TranData.TransactionStatus = TransStatus.Approved;
                            if (fields[4].ToLower().Equals("rejected")) TranData.TransactionStatus = TransStatus.Rejected;
                            if (fields[4].ToLower().Equals("done")) TranData.TransactionStatus = TransStatus.Done;
                        }
                        else
                        {
                            TranData.TransactionStatus = TransStatus.None;
                        }
                        TranData.CreatedBy = "Wai";
                        TranData.CreatedDate = DateTime.Today;
                        TranData.ModifiedBy = "Wai";
                        TranData.ModifiedDate = DateTime.Today;

                        PaymentData dtlPayment = new PaymentData();
                        dtlPayment.Amount = fields[1] == null ? 0 : Convert.ToDecimal(fields[1]);
                        dtlPayment.CurrencyCode = fields[2] == null ? "" : fields[2];

                        TranData.PaymentDetails = dtlPayment;
                        Data.Add(TranData);
                    }
                }
                
                foreach (var item in Data)
                {
                    DynamicParameters param = new DynamicParameters();
                    param.Add(@"@TransactionId", item.TransactionId);
                    param.Add(@"@TransactionDate", item.TransactionDate);
                    param.Add(@"@Status", item.TransactionStatus);
                    param.Add(@"@Amount", item.PaymentDetails.Amount);
                    param.Add(@"@CurrencyCode", item.PaymentDetails.CurrencyCode);
                    param.Add(@"@CreatedBy", item.CreatedBy);
                    param.Add(@"@CreatedDateTime", item.CreatedDate);
                    param.Add(@"@ModifiedBy", item.ModifiedBy);
                    param.Add(@"@ModifiedBy", item.ModifiedDate);
                    DapperCRUD.ExecuteWithoutReturn("AddNewTransaction", param);
                }               
                return "File Uploaded Successfully!!";
            }
            catch (Exception ex)
            {
                return string.Format("Failed to upload file! {0}",ex.Message);
            } 
        }
        private static string XMLFileReader(string filePath)
        {

            try
            {
                List<TransactionData> Data = new List<TransactionData>();
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);
                foreach (XmlNode nodeProcessor in xmlDoc.DocumentElement.SelectNodes("Transaction"))
                {
                    TransactionData TranData = new TransactionData();
                    if (nodeProcessor.Attributes["Id"] != null)
                    {
                        TranData.TransactionId = nodeProcessor.Attributes["Id"].Value;
                        var TransactionDate = nodeProcessor.SelectSingleNode("TransactionDate");
                        if (TransactionDate != null && TransactionDate.NodeType == XmlNodeType.Element)
                        {
                            TranData.TransactionDate = Convert.ToDateTime(TransactionDate.InnerText);
                        }
                        var Status = nodeProcessor.SelectSingleNode("Status");
                        if (Status != null && Status.NodeType == XmlNodeType.Element)
                        {
                            if (Status.InnerText.ToLower().Equals("approved")) TranData.TransactionStatus = TransStatus.Approved;
                            if (Status.InnerText.ToLower().Equals("rejected")) TranData.TransactionStatus = TransStatus.Rejected;
                            if (Status.InnerText.ToLower().Equals("done")) TranData.TransactionStatus = TransStatus.Done;
                        }
                        else
                        {
                            TranData.TransactionStatus = TransStatus.None;
                        }
                        TranData.CreatedBy = "Wai";
                        TranData.CreatedDate = DateTime.Today;
                        TranData.ModifiedBy = "Wai";
                        TranData.ModifiedDate = DateTime.Today;

                        var PaymentDetails = nodeProcessor.SelectSingleNode("PaymentDetails");
                        if (PaymentDetails != null)
                        {
                            PaymentData dtlPayment = new PaymentData();
                            foreach (XmlNode nodeArg in PaymentDetails.ChildNodes)
                            {
                                if (nodeArg.NodeType == XmlNodeType.Element)
                                {
                                    if (nodeArg.Name.ToLower().Equals("amount"))
                                    {
                                        dtlPayment.Amount = nodeArg.InnerXml == null ? 0 : Convert.ToDecimal(nodeArg.InnerXml);
                                    }
                                    if (nodeArg.Name.ToLower().Equals("currencycode"))
                                    {
                                        dtlPayment.CurrencyCode = nodeArg.InnerXml == null ? "" : nodeArg.InnerXml;
                                    }
                                }
                            }
                            TranData.PaymentDetails = dtlPayment;
                        }
                        Data.Add(TranData);
                    }
                }
                return "File Uploaded Successfully!!";
            }
            catch (Exception ex )
            {
                return string.Format("Failed to upload file! {0}", ex.Message);
            }            
        }
    
    }
}
