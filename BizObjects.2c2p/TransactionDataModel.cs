﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BizObjects._2c2p
{   
    public enum TransStatus
    {
        None = 0,
        Approved =1 , 
        Rejected =2,
        Done = 3
    }
    public class TransactionData
    {       
        public string TransactionId { get; set; }       
    
        public DateTime TransactionDate { get; set; }

        public PaymentData PaymentDetails { get; set; }
    
        public TransStatus TransactionStatus { get; set; }

        public string CreatedBy { get; set; }
              
        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }
        
        public DateTime ModifiedDate { get; set; }
    }
    public class PaymentData
    {  
              
        public decimal Amount { get; set; }

        public string CurrencyCode { get; set; }      
       
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }
    }

    public class Response_TransactionData
    {
        public string Id { get; set; }

        public string Payment { get; set; }

        public string Status { get; set; }

    }

}
