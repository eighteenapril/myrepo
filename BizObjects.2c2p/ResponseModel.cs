﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizObjects._2c2p
{
  
    public class OKResponse
    {
        public string status { get; set; }
        public object results { get; set; }
    }
    public class WarningResponse
    {
        public string status { get; set; }
        public object results { get; set; }
    }
    public class ErrorResponse
    {
        public string status { get; set; }
        public string errorCode { get; set; }
        public string errorMessage { get; set; }
        public Dictionary<String, object> errorDetails { get; set; }
    }     
}
