﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BizObjects._2c2p
{
    public enum UploadStatus
    {
        Successful,
        Failed
    }
    public class DataFile
    {           
        public string Id { get; set; }

        public string FileName { get; set; }
      
        public string FileExtension { get; set; }
         
        public string FilePath { get; set; }        
       
        public UploadStatus Status { get; set; }
     
        public UploadStatus ErrorMessage { get; set; }     
        
        public string CreatedBy { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string ModifiedBy { get; set; }
         
        public DateTime ModifiedDate { get; set; }

    }

}


