﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BizLogic._2c2p;

namespace Portal._2c2p.Controllers
{
    public class UploadFileController : Controller
    {
        
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {                  
                    ViewBag.Message = SaveFile(file);
                }
                return View();
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }
        private string SaveFile(HttpPostedFileBase file2upload)
        {
            try
            {
                string fileExtension = Path.GetExtension(file2upload.FileName);
                int fileSize = file2upload.ContentLength;
                if (string.IsNullOrEmpty(fileExtension))
                {
                    return "Unknown format!";
                }
                else if (fileExtension.ToLower() != ".csv" && fileExtension.ToLower() != ".xml")
                {
                    return "Invalid file format! Only file with csv or xml extension are allowed.";
                }
                else if (fileSize > 1048576)
                {
                    return "Max file size (1MB) exceeded!";
                }
                else
                {
                    string _FileName = Path.GetFileName(file2upload.FileName);
                    string _path = Path.Combine(Server.MapPath("App_Data/UploadedFiles"), _FileName);
                    file2upload.SaveAs(_path);
                    return DataFileBL.ProcessFile(_path,fileExtension);
                   
                }
            }
            catch (Exception ex)
            {
                return "File upload failed!!";
            }
           
        }

    }
}