/****** Object:  StoredProcedure [dbo].[TransactionDataList]    Script Date: 05/14/2020 17:12:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionDataList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransactionDataList]
GO

USE [MyDB]
GO

/****** Object:  StoredProcedure [dbo].[TransactionDataList]    Script Date: 05/14/2020 17:12:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[TransactionDataList]
As
/*
Exec [TransactionDataList]
*/
Begin

Select 
Id = t.TransactionId,
Payment = Convert(nvarchar(20),p.Amount) + ' ' + p.CurrencyCode,
CASE t.Status WHEN '1' THEN 'A'
    WHEN '2' THEN 'R'
    WHEN '3' THEN 'D'
    ELSE ''
END AS [Status] 
from tblTransactions t inner join tblTransactionPayment p on t.TransactionId = p.TransactionId

 
End
GO


