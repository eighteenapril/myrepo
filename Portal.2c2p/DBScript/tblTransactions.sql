/****** Object:  Table [dbo].[tblTransactions]    Script Date: 05/14/2020 17:06:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[tblTransactions]
GO

USE [MyDB]
GO

/****** Object:  Table [dbo].[tblTransactions]    Script Date: 05/14/2020 17:06:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblTransactions](
	[Id] [uniqueidentifier] NOT NULL,
	[TransactionId] [nvarchar](20) NOT NULL,
	[TransactionDate] [datetime] NULL,
	[Status] [char](1) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblTransactions] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


