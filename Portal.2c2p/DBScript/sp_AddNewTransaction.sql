
/****** Object:  StoredProcedure [dbo].[AddNewTransaction]    Script Date: 05/14/2020 17:11:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddNewTransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AddNewTransaction]
GO

USE [MyDB]
GO

/****** Object:  StoredProcedure [dbo].[AddNewTransaction]    Script Date: 05/14/2020 17:11:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[AddNewTransaction] 

@TransactionId nvarchar(20), 
@TransactionDate DateTime,
@Status char(1),
@Amount decimal(18, 2),
@CurrencyCode char(3),
@CreatedBy nvarchar(50),
@CreatedDateTime DateTime,
@ModifiedBy nvarchar(50),
@ModifiedDate DateTime

As

Begin 

Insert Into tblTransactions (Id,TransactionId,TransactionDate,[Status],CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) 
values (NEWID(), @TransactionId,@TransactionDate,@Status, @CreatedBy, @CreatedDateTime,@ModifiedBy,@ModifiedDate)

 Insert Into  tblTransactionPayment (Id,TransactionId,Amount,CurrencyCode) 
 values (NEWID(), @TransactionId, @Amount, @CurrencyCode)
 
 End
 

GO


