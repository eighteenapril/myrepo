/****** Object:  StoredProcedure [dbo].[TransactionDataByStatus]    Script Date: 05/14/2020 17:12:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionDataByStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[TransactionDataByStatus]
GO

USE [MyDB]
GO

/****** Object:  StoredProcedure [dbo].[TransactionDataByStatus]    Script Date: 05/14/2020 17:12:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create Proc [dbo].[TransactionDataByStatus] @Status char(1)
As
/*
Exec [TransactionDataList]
*/
Begin

Select 
Id = t.TransactionId,
Payment = Convert(nvarchar(20),p.Amount) + ' ' + p.CurrencyCode,
CASE t.[Status] WHEN '1' THEN 'A'
    WHEN '2' THEN 'R'
    WHEN '3' THEN 'D'
    ELSE ''
END AS [Status] 
From tblTransactions t inner join tblTransactionPayment p on t.TransactionId = p.TransactionId
Where t.[Status] = @Status
 
End
GO


