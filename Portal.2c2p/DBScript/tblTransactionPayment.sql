
/****** Object:  Table [dbo].[tblTransactionPayment]    Script Date: 05/14/2020 17:05:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblTransactionPayment](
	[Id] [uniqueidentifier] NOT NULL,
	[TransactionId] [nvarchar](20) NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[CurrencyCode] [char](3) NULL,
 CONSTRAINT [PK_tbTransactionPayment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[TransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


