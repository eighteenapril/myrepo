﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess._2c2p
{
    public class DapperCRUD
    {
        private static string connectionString = @"Data Source=localhost;Initial Catalog=MyDB;User ID=sa;Password=P@44w0rd;";

        //DapperCRUD.ExecuteWithoutReturn(spName,param) 
        public static void ExecuteWithoutReturn(string spName, DynamicParameters param = null)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                sqlCon.Execute(spName, param, commandType: CommandType.StoredProcedure);
            }
        }

        //DapperCRUD.ExecuteReturnScalar<int>(spName,param) 
        public static T ExecuteReturnScalar<T>(string spName, DynamicParameters param = null)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                return (T)Convert.ChangeType(sqlCon.ExecuteScalar(spName, param, commandType: CommandType.StoredProcedure), typeof(T));
            }
        }

        //DapperCRUD.ReturnList<T> 
        public static IEnumerable<T> ReturnList<T>(string spName, DynamicParameters param = null)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                return sqlCon.Query<T>(spName, param, commandType: CommandType.StoredProcedure);

            }
        }
    }
}
